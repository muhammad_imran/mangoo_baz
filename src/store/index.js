import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
    advice({commit}){
      console.log(commit)
      var random = Math.floor(Math.random() * 9e9);
      return axios({

        method:'GET',
        url:`https://api.adviceslip.com/advice?${random}`
      })
    }



  },
  modules: {
  }
})
